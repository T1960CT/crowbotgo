package main

import (
	"flag"
	"fmt"
	"math/rand"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	"gitlab.com/T1960CT/crowbot-go/commands"

	"github.com/bwmarrin/discordgo"
)

const prefix = ";"

// Pass it through command flag so the Service can handle secrets
var Token string

func init() {
	rand.Seed(time.Now().Unix())
	flag.StringVar(&Token, "t", "", "Bot Token")
	flag.Parse()
}

func main() {

	// Create a new Discord session using the provided bot token.
	dg, err := discordgo.New("Bot " + Token)
	if err != nil {
		fmt.Println("error creating Discord session,", err)
		return
	}

	// Register the messageCreate func as a callback for MessageCreate events.
	dg.AddHandler(messageCreate)

	// In this example, we only care about receiving message events.
	dg.Identify.Intents = discordgo.IntentsGuildMessages

	// Open a websocket connection to Discord and begin listening.
	err = dg.Open()
	if err != nil {
		fmt.Println("error opening connection,", err)
		return
	}

	dg.UpdateStatusComplex(discordgo.UpdateStatusData{
		Activities: []*discordgo.Activity{{Type: 3, Name: "you sleep."}},
	})

	// Wait here until CTRL-C or other term signal is received.
	fmt.Println("Bot is now running.  Press CTRL-C to exit.")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc

	// Cleanly close down the Discord session.
	dg.Close()
}

func messageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {
	if m.Author.ID == s.State.User.ID {
		return
	}

	ctx := commands.CTX{
		Prefix:  prefix,
		Session: s,
		Message: m,
	}

	if cmdParse(ctx, "forage") {
		commands.Forage(ctx)
	} else if cmdParse(ctx, "caw") {
		commands.Caw(ctx)
	} else if cmdParse(ctx, "help") {
		commands.Help(ctx)
	}
}

func cmdParse(ctx commands.CTX, cmd string) bool {
	return strings.HasPrefix(ctx.Message.Content, ctx.Prefix+cmd)
}
