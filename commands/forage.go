package commands

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math/rand"
	"os"
	"strings"
	"time"

	"golang.org/x/exp/slices"
)

type Forageable struct {
	Name   string `json:"name"`
	Desc   string `json:"desc"`
	Period string `json:"period"`

	Strong   []string `json:"strong_harvest"`
	Possible []string `json:"possible_harvest"`
}

type ForageWiki struct {
	Name string `json:"name"`
	Url  string `json:"url"`
}

func Forage(ctx CTX) {
	tokens := strings.Split(strings.TrimPrefix(ctx.Message.Content, ctx.Prefix), " ")

	// assemble current foraging information
	currentSeason := getCurrentSeason()

	// assume only token is command name
	if len(tokens) == 1 {
		// build text2image file
		err := buildText2Image(currentSeason)
		if err != nil {
			fmt.Println(err)
			return
		}
		sendImg(ctx)
	}

	// we only have single word arguments
	if len(tokens) == 2 {
		switch tokens[1] {
		case "random":
			item := currentSeason.Strong[rand.Intn(len(currentSeason.Strong))]
			link := getLink(readLinks(), item).Url
			ctx.Session.ChannelMessageSend(ctx.Message.ChannelID, item+"! ("+link+")")
		default:
			// help foraging
		}
	}
}

func readLinks() []ForageWiki {
	index, err := os.Open("foragingwiki.json")
	if err != nil {
		fmt.Errorf("%s", err)
		return nil
	}
	defer index.Close()

	byteResult, _ := ioutil.ReadAll(index)

	var res []ForageWiki
	json.Unmarshal([]byte(byteResult), &res)
	return res
}

func readList() []Forageable {
	foraging, err := os.Open("foraging.json")
	if err != nil {
		fmt.Errorf("%s", err)
		return nil
	}
	defer foraging.Close()

	byteResult, _ := ioutil.ReadAll(foraging)

	var res []Forageable
	json.Unmarshal([]byte(byteResult), &res)
	return res
}

func getCurrentSeason() Forageable {
	_, mm, dd := time.Now().Date()
	var fList = readList()

	switch mm {
	case time.January:
		return getItem(fList, "mid_winter")
	case time.February:
		if dd <= 14 {
			return getItem(fList, "mid_winter")
		}
		return getItem(fList, "late_winter")
	case time.March:
		if dd <= 24 {
			return getItem(fList, "late_winter")
		}
		return getItem(fList, "early_spring")
	case time.April:
		if dd <= 25 {
			return getItem(fList, "early_spring")
		}
		return getItem(fList, "mid_spring")
	case time.May:
		if dd <= 10 {
			return getItem(fList, "mid_spring")
		}
		return getItem(fList, "late_spring")
	case time.June:
		if dd <= 5 {
			return getItem(fList, "late_spring")
		}
		return getItem(fList, "early_summer")
	case time.July:
		return getItem(fList, "mid_summer")
	case time.August:
		if dd <= 10 {
			return getItem(fList, "mid_summer")
		}
		return getItem(fList, "late_summer")
	case time.September:
		if dd >= 10 {
			return getItem(fList, "early_fall")
		}
		return getItem(fList, "late_summer")
	case time.October:
		if dd >= 10 {
			return getItem(fList, "late_fall")
		}
		return getItem(fList, "early_fall")
	case time.November:
		if dd <= 15 {
			return getItem(fList, "late_fall")
		}
		return getItem(fList, "early_winter")
	case time.December:
		return getItem(fList, "early_winter")
	default:
		return Forageable{}
	}
}

func getItem(f []Forageable, m string) Forageable {
	return f[slices.IndexFunc(f, func(f Forageable) bool { return f.Name == m })]
}

func getLink(w []ForageWiki, m string) ForageWiki {
	return w[slices.IndexFunc(w, func(w ForageWiki) bool { return w.Name == m })]
}
