package commands

import "github.com/bwmarrin/discordgo"

type CTX struct {
	Prefix  string
	Session *discordgo.Session
	Message *discordgo.MessageCreate
}

func embed(ctx CTX) *discordgo.MessageEmbed {
	return &discordgo.MessageEmbed{
		Color:     0x23d160,
		Thumbnail: &discordgo.MessageEmbedThumbnail{URL: ctx.Session.State.User.AvatarURL("")},
		Footer:    &discordgo.MessageEmbedFooter{Text: "CrowBot by: amgry crow"},
	}
}
