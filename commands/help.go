package commands

import (
	"strings"

	"github.com/bwmarrin/discordgo"
)

func Help(ctx CTX) {
	tokens := strings.Split(strings.TrimPrefix(ctx.Message.Content, ctx.Prefix), " ")
	e := embed(ctx)

	h := func() {
		e.Title = "Help Command List"
		e.Fields = []*discordgo.MessageEmbedField{
			{Name: "__Just for Fun__", Value: "`!help caw`", Inline: true},
			{Name: "__Utilities__", Value: "`!help forage`", Inline: true},
			{Name: "__Not Helping?__", Value: "Post a message in CrowBot's\n`general` or `bugs-issues` channels!", Inline: false},
		}
	}

	// we only have single word arguments
	if len(tokens) == 2 {
		switch tokens[1] {
		case "forage":
			e.Title = "Help Forage"
			e.Description = "Forage shows foragable items listed in two groups:\nStrong Harvest (very likely to find)\n" +
				"Possible Harvest (going out of/into season)\n_Note: Only applies to North America._"
			e.Fields = []*discordgo.MessageEmbedField{
				{Name: "Command(s)", Value: "`!forage`\n`!forage random`  (picks a Strong Harvest)"},
			}
		case "caw":
			e.Title = "Help Caw"
			e.Description = "Caw triggers the murder."
			e.Fields = []*discordgo.MessageEmbedField{
				{Name: "Command(s)", Value: "`!caw`"},
			}
		default:
			h()
		}
	} else {
		// in all other cases, send regular help
		h()
	}

	ctx.Session.ChannelMessageSendEmbed(ctx.Message.ChannelID, e)
}
