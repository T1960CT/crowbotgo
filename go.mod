module gitlab.com/T1960CT/crowbot-go

go 1.18

require (
	github.com/bwmarrin/discordgo v0.24.0
	golang.org/x/exp v0.0.0-20220407100705-7b9b53b0aca4
	golang.org/x/image v0.0.0-20220321031419-a8550c1d254a
)

require (
	github.com/gorilla/websocket v1.4.2 // indirect
	golang.org/x/crypto v0.0.0-20210421170649-83a5a9bb288b // indirect
	golang.org/x/sys v0.0.0-20211019181941-9d821ace8654 // indirect
)
